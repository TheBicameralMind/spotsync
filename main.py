import signal
import sys
from argparse import ArgumentParser
from shutil import copy2
from time import sleep
from typing import Dict

from loguru import logger

from spotsync import __version__ as spotversion
from spotsync.config import (
    CACHE_PATH,
    SCOPE,
    ConfigLocation,
    get_config,
    process_cfg,
    show_config,
)
from spotsync.models import Local, Spotify, StateManager
from spotsync.sync import Differ, Resolver

SP_STATE = Spotify
LC_STATE = Local


def signal_handler(signal, frame):
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)


def monitor(config: Dict, dry_run: bool):
    lman = LC_STATE(config)
    rman = SP_STATE(config)
    differ = Differ(config)
    resolver = Resolver(config, dry_run)

    while True:
        _loop(lman, rman, differ, resolver)
        sleep(config["remote"]["poll_delay"])


def check_once(config: Dict, dry_run: bool):
    lman = LC_STATE(config)
    rman = SP_STATE(config)
    differ = Differ(config)
    resolver = Resolver(config, dry_run)

    _loop(lman, rman, differ, resolver)


def auth(config: Dict):
    from spotipy import Spotify
    from spotipy.oauth2 import SpotifyOAuth

    spot = Spotify(
        auth_manager=SpotifyOAuth(
            scope=SCOPE, **config["remote"]["auth"], open_browser=False
        )
    )
    me = spot.me()

    logger.info(f"Authenticated as {me['display_name']} ({me['id']})")

    copy2(".cache", CACHE_PATH / ".cache")


def _loop(
    local: StateManager,
    remote: StateManager,
    differ: Differ,
    resolver: Resolver,
):
    localState = local.state()
    logger.debug(f"Local: {localState}")

    remoteState = remote.state()
    logger.debug(f"Remote: {remoteState}")

    diff = differ.diff(remoteState, localState)

    diff.print()

    if diff.changes():
        resolver.resolve(diff)


def setup_args():
    parser = ArgumentParser()
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        help="Performs a dry run -- everything except the actual download/removal of songs",
    )
    parser.add_argument("-c", "--config", help="Alternative location for config.json")
    parser.add_argument(
        "-j",
        "--json-schema",
        help="Alternative location for config.schema.json (used for config validation)",
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-m",
        "--monitor",
        action="store_true",
        help="Enables monitor mode; causes SpotSync to run in the background until stopped, syncing at an interval defined in the config file",
    )
    group.add_argument(
        "-s", "--sync-once", action="store_true", help="Performs single sync and exits"
    )
    group.add_argument(
        "-a",
        "--auth",
        action="store_true",
        help="Used to authenticate with Spotify API and extract the cached credentials (due to complications with Docker compose)",
    )
    group.add_argument(
        "-l",
        "--debug",
        action="store_true",
        help="Loop no-ops so a debugger can be attached to the container",
    )
    return parser.parse_args()


@logger.catch
def main():
    args = setup_args()

    logger.info(f"SpotSync version {spotversion}")

    if any((args.monitor, args.sync_once, args.auth)):
        config_result = get_config(ConfigLocation(args.config, args.json_schema))
        if not config_result["valid"]:
            logger.error(f"Invalid config: {config_result['config']}")
            sys.exit(1)

        # i hate this but it's as close as i get to rust result enums :/
        config = config_result["config"]
        show_config(config)
        process_cfg(config)

    if args.debug:
        logger.debug("Entering debug mode: looping until debugger attachment")
        while True:
            sleep(5)
    elif args.monitor:
        logger.info("Entering monitor mode")
        monitor(config, args.dry_run)
    elif args.sync_once:
        logger.info("Performing single sync")
        check_once(config, args.dry_run)
    elif args.auth:
        logger.info("Authenticating with Spotify")
        auth(config)


if __name__ == "__main__":
    main()
