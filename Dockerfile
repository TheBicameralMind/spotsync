# FROM python:3.10-slim-bullseye AS base

# ENV PYTHONFAULTHANDLER=1 \
#     PYTHONHASHSEED=random \
#     PYTHONUNBUFFERED=1

# WORKDIR /app

# FROM base as builder

# ENV PIP_DEFAULT_TIMEOUT=100 \
#     PIP_DISABLE_PIP_VERSION_CHECK=1 \
#     PIP_NO_CACHE_DIR=1 \
#     POETRY_VERSION=1.4.0

# RUN pip install "poetry==$POETRY_VERSION"
# RUN python -m venv /venv

# COPY pyproject.toml poetry.lock ./
# RUN poetry export -f requirements.txt | /venv/bin/pip install -r /dev/stdin

# COPY . .
# RUN poetry build && /venv/bin/pip install dist/*.whl

# FROM base as final

# RUN apt update && apt install ffmpeg
# COPY --from=builder /venv /venv
# ENTRYPOINT "/venv/bin/python"
# CMD ["main.py"]

FROM python:3.10-slim-bullseye

RUN apt-get update && apt-get install --no-install-recommends -y \
    ffmpeg \
    git \
    gosu \
    && rm -rf /var/lib/apt/lists/*

ENV PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1 \
    POETRY_VERSION=1.4.0

RUN pip install "poetry==$POETRY_VERSION"

RUN git clone --depth=1 https://gitlab.com/TheBicameralMind/spotsync.git /app

RUN useradd -u 911 -U -d /home/user -s /bin/false user &&\
    usermod -G users user && mkdir -p /home/user && chown user:user /home/user

WORKDIR /app

# COPY ./config ./config
# COPY pyproject.toml poetry.lock ./
RUN python -m venv .venv
RUN poetry export -f requirements.txt | .venv/bin/pip install -r /dev/stdin

# COPY spotsync/ spotsync/
# COPY main.py README.md ./
RUN poetry build && .venv/bin/pip install dist/*.whl

RUN chmod +x ./run.sh

# RUN sed -i 's/server = HTTPServer(("127.0.0.1", port), handler)/server = HTTPServer(("0.0.0.0", port), handler)/g' .venv/lib/python3.10/site-packages/spotipy/oauth2.py

ENTRYPOINT ["/app/run.sh"]