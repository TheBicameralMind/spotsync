# SpotSync
Local backups of your selected Spotify liked tracks, liked albums, and playlists, sourced from YouTube. 

Uses [Spotipy](https://github.com/spotipy-dev/spotipy) for Spotify info and [spotDL](https://github.com/spotDL/spotify-downloader) (backed by [yt-dlp](https://github.com/yt-dlp/yt-dlp)) for fetching songs from YouTube.

# Installation

## 1. Create a Spotify App

1. Visit [developer.spotify.com](https://developer.spotify.com/) and log in using your Spotify credentials
2. Visit the [dashboard](https://developer.spotify.com/dashboard) and create a new app (called whatever you'd like)
3. Click "Settings," then "View client secret"
4. Copy the Client ID and the Client Secret into your config file
5. Then, at the bottom of the page, click "Edit"
6. Add `http://localhost:8123/` as a new URI redirect

## 2. Cache Your Spotify Credentials (or, I Hate OAuth)

1. Copy [`config/config.json`](config/config.json) (or create and fill out your own) to `$PROJECT_DIRECTORY`
2. Copy [`docker-compose.yml`](./docker-compose.yml) (or create and fill out your own) to `$PROJECT_DIRECTORY` 
1. In PowerShell or \$\{Linux Shell\}, from `$PROJECT_DIRECTORY`, run `docker run --rm -it -v "$(pwd)/config.json:/app/config.json" -v "$(pwd):/app/cache" registry.gitlab.com/thebicameralmind/spotsync --auth`
2. Go to the URL it prints out, authenticate with Spotify, and copy the URL that you are redirected to
3. Enter that URL into the terminal

After this, you should have a `.cache` file alongside your `config.json` file. If you are using Docker Compose (recommended), you should have a `docker-compose.yml` file as well. 

# Usage
Run the container using: 
* `docker compose up -d spotsync` or 
* `docker run -v ${YOUR_MUSIC_DIR}:/music -v ${YOUR_CONFIG_PATH}:/app/config.json -v ${YOUR_CACHE_PATH}:/app/.cache --name spotsync -e LOGURU_LEVEL=INFO registry.gitlab.com/thebicameralmind/spotsync:latest --monitor`

## CLI Flags

| Flag (short / long) | Meaning |
|---------------------|---------|
| `-d / --dry-run`    | Performs a dry run -- everything runs normally except the actual download/removal of songs |
| `-c / --config`     | Alternative location for config.json; defaults to `/app/config.json` | 
| `-j / --json-schema`| Alternative location for config.schema.json (used for config validation); defaults to `/app/config/config.schema.json` |
| `-m / --monitor`    | Enables monitor mode; causes SpotSync to run in the background until stopped, syncing at the interval defined in the config file |
| `-s / --sync-once`  | Performs single sync and exits |
| `-a / --auth`       | Authenticates with Spotify and exits (used to cache Spotify credentials for Docker Compose) |

> ⚠️ The `-m`, `-s`, `-a` options are mutually exclusive. 

CLI flags can be appended to the end of a `docker run` command, or specified in the `command` field of the `docker-compose.yml` file. 

## Configuration

Config is handled by the `config.json` file. An example has been provided in [`config/config.json`](config/config.json). Config is validated using [`jsonschema`](https://github.com/python-jsonschema/jsonschema) against the schema provided by [`config/config.schema.json`](config/config.schema.json). 

Docs for the entire config file can be found in the [`docs`](docs/) folder as an interactive webpage (servable through something like Python's [`http.server`](https://docs.python.org/3/library/http.server.html)) or a static Markdown file. 

Log level can also be configured by changing the container's `LOGURU_LEVEL` environment variable. The default value is `DEBUG` if unset. 

### Allow / Deny Lists

The config offers both an allow list and a deny list for all categories. Both lists take either exact matches or the wildcard (`*`) for the respective category (e.g. playlist names for `playlists`, album names for `albums`). 

The deny list takes precendence over the allow list (e.g. if Rick Astley's `Never Gonna Give You Up" is in your saved songs deny list, it will not be downloaded, even if it or the wildcard is in your saved songs allow list). Including a wildcard in a deny list will result in no downloads for that category. 

### Examples
*An example of an inclusive configuration for `albums`:*
```json
"albums": {
    "allow": [
        "El Camino", 
        "Costello Music",
        "Is This It"
    ], 
    "deny": []
}
```
*An example of an exclusive configuration for `songs`:*
```json
"songs": {
    "allow": ["*"], 
    "deny": [
        "27 Kids", 
        "Hurry, Hurry", 
        "Breaking up My Bones", 
        "A Jagged Gorgeous Winter"
    ]
}
```