#!/bin/bash

export PUID=${PUID:-911}
export PGID=${PGID:-911}

groupmod -o -g "$PGID" user
usermod -o -u "$PUID" user

echo '---------------------
GID/UID               
---------------------'
echo "User uid:    $(id -u user)
User gid:    $(id -g user)
---------------------"
chown user:user /app
chown user:user /music

gosu user .venv/bin/python main.py $*