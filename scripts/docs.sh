#!/bin/bash

source .venv/bin/activate

generate-schema-doc config/config.schema.json docs
generate-schema-doc --config template_name=md config/config.schema.json docs