#!/bin/bash

helpFunction()
{
   echo ""
   echo "Usage: $0 -t TAG-NAME -p"
   echo -e "\t-t Tag to use when building the image"
   echo -e "\t-p Whether to push the image to GitLab container registry"
   exit 1 # Exit script after printing help
}

while getopts "t:p" opt
do
   case "$opt" in
      t ) TAGNAME="$OPTARG" ;;
      p ) PUSH=true ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

if [ -z $PUSH ]
then
    PUSH=false
fi

# Print helpFunction in case parameters are empty
if [ -z "$TAGNAME" ] || [ -z "$PUSH" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

echo "Tag: $TAGNAME"
echo "Push: $PUSH"

docker build -t "registry.gitlab.com/thebicameralmind/spotsync:$TAGNAME" .

if $PUSH 
then
    docker push "registry.gitlab.com/thebicameralmind/spotsync:$TAGNAME"
fi