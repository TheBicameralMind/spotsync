try:
    from .benchmark import timer
except ImportError:
    from benchmark import timer

from spotsync.models import Spotify, SpotifyMultiProcess, SpotifyMultiThread


def test_spotify(name="test_spotify"):
    sp = Spotify()

    with timer(name):
        sp.state()

    assert True


def test_spotify_mt(name="test_spotify_th"):
    sp = SpotifyMultiThread()

    with timer(name):
        sp.state()

    assert True


def test_spotify_mp(name="test_spotify_mp"):
    sp = SpotifyMultiProcess()

    with timer(name):
        sp.state()

    assert True


if __name__ == "__main__":
    funcs = [
        value
        for key, value in locals().items()
        if callable(value) and value.__module__ == __name__ and key.startswith("test_")
    ]
    [f() for f in funcs]
