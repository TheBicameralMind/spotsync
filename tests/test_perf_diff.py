import pickle

from spotsync.sync import diff

try:
    from .benchmark import timer
except ImportError:
    from benchmark import timer

import os


def load_states():
    try:
        os.chdir("tests")
    except:
        ...
    with open("local.pk", "rb") as inlocal, open("remote.pk", "rb") as inremote:
        local = pickle.load(inlocal)
        remote = pickle.load(inremote)

    return local, remote


def test_diff(name="test_diff"):
    local, remote = load_states()

    with timer(name):
        diff(remote, local)

    assert True


if __name__ == "__main__":
    funcs = [
        value
        for key, value in locals().items()
        if callable(value) and value.__module__ == __name__ and key.startswith("test_")
    ]
    [f() for f in funcs]
