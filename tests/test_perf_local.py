try:
    from .benchmark import timer
except ImportError:
    from benchmark import timer
from spotsync.models import Local, LocalMultiProcess


def test_local(name="test_local"):
    l = Local()

    with timer(name):
        l.state()

    assert True


def test_local_mp(name="test_local_mp"):
    l = LocalMultiProcess()
    l_ = Local()
    ll_ = l_.state()

    with timer(name):
        ll = l.state()

    assert ll == ll_


if __name__ == "__main__":
    funcs = [
        value
        for key, value in locals().items()
        if callable(value) and value.__module__ == __name__ and key.startswith("test_")
    ]
    [f() for f in funcs]
