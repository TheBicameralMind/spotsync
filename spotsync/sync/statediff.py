from collections import Counter, defaultdict
from dataclasses import dataclass
from enum import Enum, auto
from typing import Dict, List, Union

from loguru import logger

from ..config import ADList
from ..models import Song, State


class AutoName(Enum):
    def _generate_next_value_(name, *_):
        return name


class Actions(AutoName):
    DIR_DELETE = "Remove"
    DIR_CREATE = "Create"
    SONG_DELETE = "Delete"
    SONG_DOWNLOAD = "Download"


@dataclass
class Diff:
    subject: Song
    action: Actions


class StateDiff:
    albums: Dict[str, Union[Diff, List[Diff]]] = defaultdict(list)
    playlists: Dict[str, Union[Diff, List[Diff]]] = defaultdict(list)
    songs: List[Diff] = []
    urlmap: Dict[str, str]

    def changes(self) -> bool:
        return len(self.songs) > 0 or len(self.playlists) > 0 or len(self.albums) > 0

    def __repr__(self) -> str:
        albumNum = sum(
            len(self.albums[a]) if isinstance(self.albums[a], List) else 1
            for a in self.albums
        )
        playlistNum = sum(
            len(self.playlists[p]) if isinstance(self.playlists[p], List) else 1
            for p in self.playlists
        )

        return f"StateDiff(albums={albumNum}, playlists={playlistNum}, songs={len(self.songs)}, urlmap={len(self.urlmap)})"

    def print(self) -> None:
        if self.changes():
            logger.debug("StateDiff(")

            logger.debug("  songs {")
            for s in self.songs:
                logger.debug(f'    {s.action.value} "{s.subject.name}"')
            logger.debug("  }")

            logger.debug("  albums {")
            for df in self.albums:
                if isinstance(self.albums[df], List):
                    logger.debug(f'    {df} {"{"}')
                    for d in self.albums[df]:
                        if isinstance(d.subject, Song):
                            logger.debug(f'      {d.action.value} "{d.subject.name}"')
                        else:
                            logger.debug(f'      {d.action.value} "{d.subject}"')
                    logger.debug("    }")
                else:
                    logger.debug(f"     {self.albums[df]}")
            logger.debug("  }")

            logger.debug("  playlists {")
            for df in self.playlists:
                if isinstance(self.playlists[df], List):
                    logger.debug(f'    {df} {"{"}')
                    for d in self.playlists[df]:
                        if isinstance(d.subject, Song):
                            logger.debug(f'        {d.action.value} "{d.subject.name}"')
                        else:
                            logger.debug(f'        {d.action.value} "{d.subject}"')
                    logger.debug("    }")
                else:
                    logger.debug(
                        f"    {self.playlists[df].action.value} {self.playlists[df].subject}"
                    )
            logger.debug("  }")

            logger.debug("}")

        else:
            logger.debug("StateDiff()")


class Differ:
    def __init__(self, config: Dict) -> None:
        self.albums: ADList = config["remote"]["to_sync"]["albums"]
        self.playlists: ADList = config["remote"]["to_sync"]["playlists"]
        self.songs: ADList = config["remote"]["to_sync"]["songs"]

    def diff(self, spstate: State, lcstate: State) -> StateDiff:
        logger.debug("Beginning state diff")
        stdiff = StateDiff()
        stdiff.urlmap = lcstate.urlmap

        # check remote songs
        dl_count = 0
        for rsong in spstate.songs:
            # missing song locally
            if rsong not in lcstate.songs and self.songs.keep(rsong):
                stdiff.songs.append(Diff(rsong, Actions.SONG_DOWNLOAD))
                dl_count += 1
        logmethod = logger.info if dl_count > 0 else logger.debug
        logmethod("Will download {count} saved songs", count=dl_count)

        # check local songs
        rm_count = 0
        for lsong in lcstate.songs:
            # extra song locally (or deleted from remote)
            if lsong not in spstate.songs or self.songs.remove(rsong):
                stdiff.songs.append(Diff(lsong, Actions.SONG_DELETE))
        logmethod = logger.info if rm_count > 0 else logger.debug
        logmethod("Will delete {count} saved songs", count=rm_count)

        # check remote playlists
        self._check_download(stdiff, spstate, lcstate, "playlists")
        # check remote albums
        self._check_download(stdiff, spstate, lcstate, "albums")

        # check local playlists
        self._check_delete(stdiff, spstate, lcstate, "playlists")
        # check local albums
        self._check_delete(stdiff, spstate, lcstate, "albums")

        return stdiff

    def _check_download(self, stdiff, spstate, lcstate, subject):
        c = Counter()

        remote_subj = getattr(spstate, subject)
        local_subj = getattr(lcstate, subject)
        diff_subj = getattr(stdiff, subject)
        list_subj = getattr(self, subject)

        for r_collection in remote_subj:
            # missing whole collection locally
            if r_collection not in local_subj and list_subj.keep(r_collection):
                diff_subj[r_collection].append(
                    Diff(r_collection, Actions.DIR_CREATE)
                )  # make collection directory
                # have to download all songs from that collection
                for rsong in remote_subj[r_collection]:
                    diff_subj[r_collection].append(Diff(rsong, Actions.SONG_DOWNLOAD))
                    c[r_collection] += 1

            else:  # collection exists locally
                # check for missing individual songs
                for rsong in remote_subj[r_collection]:
                    if rsong not in local_subj[r_collection]:
                        diff_subj[r_collection].append(
                            Diff(rsong, Actions.SONG_DOWNLOAD)
                        )
                        c[r_collection] += 1

        num = len(c.keys())
        total = sum(c[k] for k in c if c[k] > 0)
        logmethod = logger.info if total > 0 else logger.debug
        logmethod("Will download {k} songs across {n} {sj}", n=num, k=total, sj=subject)

    def _check_delete(self, stdiff, spstate, lcstate, subject):
        c = Counter()

        remote_subj = getattr(spstate, subject)
        local_subj = getattr(lcstate, subject)
        diff_subj = getattr(stdiff, subject)
        list_subj = getattr(self, subject)

        for l_collection in local_subj:
            # extra whole collection locally
            if l_collection not in remote_subj or list_subj.remove(l_collection):
                diff_subj[l_collection] = Diff(l_collection, Actions.DIR_DELETE)
                c[l_collection] += len(local_subj[l_collection])
            else:  # collection exists locally and remote, have to check songs
                for lsong in local_subj[l_collection]:
                    # song is in local collection but not remote
                    if lsong not in remote_subj[l_collection]:
                        diff_subj[l_collection].append(Diff(lsong, Actions.SONG_DELETE))
                        c[l_collection] += 1

        num = len(c.keys())
        total = sum(c[k] for k in c if c[k] > 0)
        logmethod = logger.info if total > 0 else logger.debug
        logmethod("Will delete {k} songs across {n} {sj}", n=num, k=total, sj=subject)
