import os
import shutil
from pathlib import Path
from typing import Dict, List, Tuple, Union

from loguru import logger
from spotdl import DownloaderOptionalOptions
from spotdl import Song as SpotSong
from spotdl import Spotdl

from ..models import Song

# from ..config import getuid, getgid
from .statediff import Actions, Diff, StateDiff


class Resolver:
    def __init__(self, config: Dict, dry_run: bool = False) -> None:
        self.config = config

        self.spotdl = self._spotdl_init()

        base = Path(self.config["local"]["basedir"])

        self.sgdir = base / "songs"
        self.pldir = base / "playlists"
        self.aldir = base / "albums"

        self.dry_run = dry_run

        # self.uid = getuid()
        # self.gid = getgid()

    def resolve(self, diff: StateDiff) -> None:
        self.sgdir.mkdir(parents=True, exist_ok=True)
        # os.chown(self.sgdir, self.uid, self.gid)
        if len(diff.songs) > 0:
            self._sync_songs(diff.songs, diff.urlmap)

        self.pldir.mkdir(parents=True, exist_ok=True)
        # os.chown(self.pldir, self.uid, self.gid)
        if len(diff.playlists) > 0:
            self._sync_songbucket(self.pldir, diff.playlists, diff.urlmap)

        self.aldir.mkdir(parents=True, exist_ok=True)
        # os.chown(self.aldir, self.uid, self.gid)
        if len(diff.albums) > 0:
            self._sync_songbucket(self.aldir, diff.albums, diff.urlmap)

    def _spotdl_init(self) -> Spotdl:
        spotdl = Spotdl(
            client_id=self.config["remote"]["auth"]["client_id"],
            client_secret=self.config["remote"]["auth"]["client_secret"],
            downloader_settings=DownloaderOptionalOptions(
                output=self.config["local"]["tmpdir"],
            ),
        )
        spotdl.downloader.progress_handler.quiet = True

        return spotdl

    def _sync_songs(self, song_diff: List[Diff], urlmap: Dict[str, Dict]) -> None:
        logger.debug("Beginning song sync")

        dl_list = []
        rm_list = []

        for diff in song_diff:
            if diff.action == Actions.SONG_DOWNLOAD:
                dl_list.append(diff.subject.url)
            if diff.action == Actions.SONG_DELETE:
                rm_list.append(diff.subject.url)

        self._download_to_directory(dl_list, self.sgdir)
        self._rm_from_directory(rm_list, self.sgdir, urlmap)

        # self.spotdl = self._recreate_spotdl()

    def _sync_songbucket(
        self,
        toplevel: Path,
        bucket_diff: Dict[str, Union[Diff, List[Diff]]],
        urlmap: Dict[str, Dict],
    ) -> None:
        logger.debug("Beginning {bucket} sync", bucket=toplevel.stem)
        for tldir in bucket_diff:  # each playlist / album
            diffs = bucket_diff[tldir]

            dl_list = []
            rm_list = []

            if isinstance(diffs, Diff):
                if diffs.action == Actions.DIR_DELETE:
                    logger.info(f"Deleting {toplevel}/{diffs.subject}")
                    if not self.dry_run:
                        shutil.rmtree(toplevel / diffs.subject)
            else:
                for diff in diffs:
                    if diff.action == Actions.DIR_CREATE:
                        logger.info(f"Creating directory {toplevel}/{diff.subject}")
                        if not self.dry_run:
                            (toplevel / diff.subject).mkdir(parents=True, exist_ok=True)
                    elif diff.action == Actions.SONG_DOWNLOAD:
                        dl_list.append(diff.subject.url)
                    elif diff.action == Actions.SONG_DELETE:
                        rm_list.append(diff.subject.url)

            self._download_to_directory(dl_list, toplevel / tldir)
            self._rm_from_directory(rm_list, toplevel / tldir, urlmap)

            # self.spotdl = self._recreate_spotdl()

    def _download_to_directory(self, song_list: List[Song], drcty: Path) -> None:
        logmethod = logger.info if len(song_list) > 0 else logger.debug
        logmethod(
            "Downloading {ln} songs to {dcty}", ln=len(song_list), dcty=str(drcty)
        )
        all_songs = list(map(lambda x: SpotSong.from_url(x), song_list))
        if not all_songs:
            return

        logger.debug(", ".join(s.name for s in all_songs))

        if self.dry_run:
            return

        dl_songs = self.spotdl.download_songs(all_songs)
        for _, dlpath in dl_songs:
            if dlpath:
                result = shutil.move(str(dlpath), drcty, copy_function=shutil.copyfile)
                logger.debug(f"Downloaded {result}")

    def _rm_from_directory(
        self, url_list: List[str], drcty: Path, urlmap: Dict[str, Dict]
    ) -> None:
        logmethod = logger.info if len(url_list) > 0 else logger.debug
        logmethod("Deleting {ln} songs from {dcty}", ln=len(url_list), dcty=drcty)

        if not url_list:
            return

        removed = []

        mapping = _traverse(urlmap, drcty.parts[2:])
        for url in url_list:
            fpath = mapping[url]
            removed.append(str(fpath))

            if not self.dry_run:
                os.remove(fpath)
                logger.debug(f"Removed {fpath}")

        logger.debug(", ".join(removed))


def _traverse(urlmap: Dict[str, Dict], parts: Tuple[str]) -> Dict:
    tld = urlmap
    for part in parts:
        tld = tld[part]
    return tld
