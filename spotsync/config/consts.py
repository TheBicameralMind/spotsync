from pathlib import Path

CONFIG_BASE = Path("/app")
CONFIG_PATH = CONFIG_BASE / "config.json"
SCHEMA_PATH = CONFIG_BASE / "config" / "config.schema.json"
CACHE_PATH = CONFIG_BASE

SCOPE = ["playlist-read-private", "user-library-read"]
