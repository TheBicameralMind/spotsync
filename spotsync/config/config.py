import json
import os
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Dict, List, Optional, Set, Union

from jsonschema import ValidationError, validate
from loguru import logger

from .consts import CONFIG_PATH, SCHEMA_PATH


@dataclass
class ConfigLocation:
    config: Optional[str]
    schema: Optional[str]


@dataclass
class ValidationResult:
    valid: bool
    reason: Optional[str]


@dataclass
class ADList:
    _allow: Set[str]
    _deny: Set[str]

    def __init__(self, allow: List, deny: List) -> None:
        self._allow = set(allow)
        self._deny = set(deny)

    def allows(self, value) -> bool:
        return (value in self._allow or "*" in self._allow) and not self.denies(value)

    def denies(self, value) -> bool:
        return "*" in self._deny or value in self._deny

    def keep(self, value) -> bool:
        return self.allows(value) and not self.denies(value)

    def remove(self, value) -> bool:
        return not self.keep(value)


def _validate(config, schema) -> ValidationResult:
    try:
        validate(config, schema)
    except ValidationError as v:
        return ValidationResult(False, v.message)
    return ValidationResult(True, None)


def _load(path: Union[str, Path]) -> Dict[str, Any]:
    with open(str(path), "r") as inf:
        return json.load(inf)


def get_config(pathinfo: ConfigLocation) -> Dict:
    try:
        config = _load(pathinfo.config if pathinfo and pathinfo.config else CONFIG_PATH)
    except FileNotFoundError:
        config = None
    schema = _load(pathinfo.schema if pathinfo and pathinfo.schema else SCHEMA_PATH)

    valid_result = _validate(config, schema)

    if valid_result.valid:
        return {"valid": True, "config": config}
    else:
        return {"valid": False, "config": valid_result.reason}


def process_cfg(cfg):
    for cat in ["playlists", "albums", "songs"]:
        cfg["remote"]["to_sync"][cat] = ADList(**cfg["remote"]["to_sync"][cat])
    return cfg


def show_config(cfg):
    logger.debug("CONFIG:")
    cfg_str = json.dumps(cfg, indent=2)
    if not cfg["log_secrets"]:
        cfg_str = cfg_str.replace(
            cfg["remote"]["auth"]["client_id"],
            "*" * len(cfg["remote"]["auth"]["client_id"]),
        ).replace(
            cfg["remote"]["auth"]["client_secret"],
            "*" * len(cfg["remote"]["auth"]["client_secret"]),
        )

    [logger.debug(l) for l in cfg_str.split("\n")]


def getuid() -> int:
    return int(os.environ.get("PUID", 911))


def getgid() -> int:
    return int(os.environ.get("PGID", 911))
