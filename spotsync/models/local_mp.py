from collections import defaultdict
from concurrent import futures
from pathlib import Path
from typing import Dict, Optional, Set

import eyed3
from loguru import logger

from ..config import ADList
from .parent import Song, State, StateManager


# nested_dict = lambda: defaultdict(nested_dict)
def nested_dict():
    return defaultdict(nested_dict)


class LocalMultiProcess(StateManager):
    def __init__(self, config: Dict) -> None:
        super().__init__(config)

        self.basedir = Path(self.config["local"]["basedir"])
        self.urlmap = nested_dict()

    def state(self):
        state = State()

        ex = futures.ProcessPoolExecutor()
        future_to_category = {
            ex.submit(self.dispatch[category], match): category
            for category, match in self.config["remote"]["to_sync"].items()
            if category in ["playlists", "albums", "songs"]
        }

        for future in futures.as_completed(future_to_category):
            category = future_to_category[future]
            setattr(state, category, future.result())

        if hasattr(self, "urlmap"):
            state.urlmap = self.urlmap

        return state

    def _get_matching_albums(self, adList: ADList) -> Dict[str, Set]:
        logger.debug("Beginning local album state collection")

        base = self.basedir / "albums"

        return self._get_matching_album_playlist(base, adList)

    def _get_matching_playlists(self, adList: ADList) -> Dict[str, Set]:
        logger.debug("Beginning local playlist state collection")

        base = self.basedir / "playlists"

        return self._get_matching_album_playlist(base, adList)

    def _get_matching_songs(self, adList: ADList) -> Set:
        logger.debug("Beginning local song state collection")

        results = set()
        with futures.ThreadPoolExecutor(thread_name_prefix="lcal_sg") as executor:
            future_to_songpath = {
                executor.submit(_process, songname): songname
                for songname in (self.basedir / "songs").glob("*")
                if adList.allows(songname.stem) and not adList.denies(songname.stem)
                # if not (
                #     songname.stem in deny_list
                #     or (allow_list != set("*") and songname.stem not in allow_list)
                # )
            }

        for future in futures.as_completed(future_to_songpath):
            path = future_to_songpath[future]
            song = future.result()

            self.urlmap["songs"][song.url] = path
            results.add(song)

        # for songname in (self.basedir / "songs").glob("*"):
        #     if songname.stem in deny_list or (
        #         allow_list != set("*") and songname.stem not in allow_list
        #     ):
        #         continue

        #     # results.add(songname.stem)
        #     songdata = eyed3.load(songname)
        #     title = songdata.tag.title
        #     url = songdata.tag.audio_source_url.replace("\x00", "")

        #     results.add(Song(title, url))

        #     self.urlmap["songs"][url] = songname

        return results

    def _get_matching_album_playlist(
        self, base: Path, adList: ADList
    ) -> Dict[str, Set]:
        results = {}

        for dirname in base.glob("*"):
            # if dirname.stem in deny_list or (
            #     allow_list != set("*") and dirname.stem not in allow_list
            # ):
            #     continue
            if adList.allows(dirname.stem) and not adList.denies(dirname.stem):
                # results[dirname.stem] = set(map(lambda x: x.stem, dirname.glob("*")))
                results[dirname.stem] = set()
                for song in dirname.glob("*"):
                    songdata = eyed3.load(song)
                    title = songdata.tag.title
                    url = songdata.tag.audio_source_url.replace("\x00", "")

                    results[dirname.stem].add(Song(title, url))

                    self.urlmap[base.stem][dirname.stem][url] = song

        return results


def _process(songname):
    songdata = eyed3.load(songname)
    title = songdata.tag.title
    url = songdata.tag.audio_source_url.replace("\x00", "")
    s = Song(title, url)
    return s
