from concurrent import futures
from typing import Callable, Dict, List, Optional, Set

import spotipy
from loguru import logger
from spotipy.oauth2 import SpotifyOAuth

from ..config import SCOPE, ADList
from .parent import Song, StateManager
from .spotify import _song_stats, _Songlist


class SpotifyMultiThread(StateManager):
    def __init__(self, config: Dict) -> None:
        super().__init__(config)

        self.spapi = spotipy.Spotify(
            auth_manager=SpotifyOAuth(scope=SCOPE, **self.config["remote"]["auth"])
        )

        # perform dummy call here to check for authentication
        # to avoid threading issues later :smile:
        self.spapi.current_user_playlists()

        self.executor = futures.ThreadPoolExecutor(thread_name_prefix="spotsync_")

    def _get_matching_albums(self, adList: ADList) -> Dict[str, Set]:
        logger.debug("Beginning remote album state collection")

        album_name_ids = self._get_all_albums()
        albums = {}

        future_to_name = {
            self.executor.submit(self._get_all_songs_al, a.id): a.name
            for a in album_name_ids
            if adList.allows(a.name) and not adList.denies(a.name)
            # if not (
            #     a.name in deny_list
            #     or (allow_list != set("*") and a.name not in allow_list)
            # )
        }
        for future in futures.as_completed(future_to_name):
            name = future_to_name[future]
            albums[name] = future.result()

        return albums

    def _get_matching_playlists(self, adList: ADList) -> Dict[str, Set]:
        logger.debug("Beginning remote playlist state collection")

        playlist_name_ids = self._get_all_playlists()
        playlists = {}

        future_to_name = {
            self.executor.submit(self._get_all_songs_pl, p.id): p.name
            for p in playlist_name_ids
            if adList.allows(p.name) and not adList.denies(p.name)
            # if not (
            #     p.name in deny_list
            #     or (allow_list != set("*") and p.name not in allow_list)
            # )
        }

        for future in futures.as_completed(future_to_name):
            name = future_to_name[future]
            playlists[name] = future.result()

        return playlists

    def _get_matching_songs(self, adList: ADList) -> Set:
        logger.debug("Beginning remote song state collection")

        allsongs = map(_song_stats, self._get_all(self.spapi.current_user_saved_tracks))

        songs = set()
        for song in allsongs:
            # if song.name in deny_list or (
            #     allow_list != set("*") and song.name not in allow_list
            # ):
            # continue
            if adList.allows(song.name) and not adList.denies(song.name):
                songs.add(song)

        return songs

    def _get_all(self, spcli_attr: Callable, spcli_args: List = None):
        if not spcli_args:
            spcli_args = []

        results = spcli_attr(*spcli_args)
        parts = results["items"]
        while results["next"]:
            results = self.spapi.next(results)
            parts.extend(results["items"])

        return parts

    def _get_all_songs_pl(self, id_: str) -> List[Song]:
        all_songs = self._get_all(self.spapi.playlist_items, (id_,))

        return self.executor.map(_song_stats, all_songs)

    def _get_all_songs_al(self, id_: str) -> List[Song]:
        all_songs = self._get_all(self.spapi.album_tracks, (id_,))

        return self.executor.map(_song_stats, all_songs)

    def _get_all_playlists(self) -> List[_Songlist]:
        raw_playlists = self._get_all(self.spapi.current_user_playlists)
        results = self.executor.map(self._plparse, raw_playlists)

        return results

    def _get_all_albums(self) -> List[_Songlist]:
        raw_albums = self._get_all(self.spapi.current_user_saved_albums)
        results = self.executor.map(self._alparse, raw_albums)

        return results
