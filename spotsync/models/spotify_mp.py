from concurrent import futures
from typing import Callable, Dict, List, Optional, Set

import spotipy
from loguru import logger
from spotipy.oauth2 import SpotifyOAuth

from ..config import SCOPE, ADList
from .parent import Song, State, StateManager
from .spotify import _song_stats, _Songlist


class SpotifyMultiProcess(StateManager):
    def __init__(self, config: Dict) -> None:
        super().__init__(config)

        self.spapi = spotipy.Spotify(
            auth_manager=SpotifyOAuth(scope=SCOPE, **self.config["remote"]["auth"])
        )

        # perform dummy call here to check for authentication
        # to avoid threading issues later :smile:
        self.spapi.current_user_playlists()

    def state(self):
        state = State()

        ex = futures.ProcessPoolExecutor()
        future_to_category = {
            ex.submit(self.dispatch[category], match): category
            for category, match in self.config["remote"]["to_sync"].items()
            if category in ["playlists", "albums", "songs"]
        }

        for future in futures.as_completed(future_to_category):
            category = future_to_category[future]
            setattr(state, category, future.result())

        if hasattr(self, "urlmap"):
            state.urlmap = self.urlmap

        return state

    def _get_matching_albums(self, adList: ADList) -> Dict[str, Set]:
        logger.debug("Beginning remote album state collection")

        with futures.ThreadPoolExecutor(thread_name_prefix="spot_al") as executor:
            album_name_ids = self._get_all_albums(executor)
            albums = {}

            future_to_name = {
                executor.submit(self._get_all_songs_al, a.id): a.name
                for a in album_name_ids
                if adList.allows(a.name) and not adList.denies(a.name)
                # if not (
                #     a.name in deny_list
                #     or (allow_list != set("*") and a.name not in allow_list)
                # )
            }

        for future in futures.as_completed(future_to_name):
            name = future_to_name[future]
            albums[name] = future.result()

        return albums

    def _get_matching_playlists(self, adList: ADList) -> Dict[str, Set]:
        logger.debug("Beginning remote playlist state collection")

        with futures.ThreadPoolExecutor(thread_name_prefix="spot_pl") as executor:
            playlist_name_ids = self._get_all_playlists(executor)
            playlists = {}

            future_to_name = {
                executor.submit(self._get_all_songs_pl, p.id): p.name
                for p in playlist_name_ids
                if adList.allows(p.name) and not adList.denies(p.name)
                # if not (
                #     p.name in deny_list
                #     or (allow_list != set("*") and p.name not in allow_list)
                # )
            }

        for future in futures.as_completed(future_to_name):
            name = future_to_name[future]
            playlists[name] = future.result()

        return playlists

    def _get_matching_songs(self, adList: ADList) -> Set:
        logger.debug("Beginning remote song state collection")
        with futures.ThreadPoolExecutor(thread_name_prefix="spot_sg") as executor:
            allsongs = executor.map(
                _song_stats, self._get_all(self.spapi.current_user_saved_tracks)
            )

        songs = set()
        for song in allsongs:
            # if song.name in deny_list or (
            #     allow_list != set("*") and song.name not in allow_list
            # ):
            #     continue
            if adList.allows(song.name) and not adList.denies(song.name):
                songs.add(song)

        return songs

    def _get_all(self, spcli_attr: Callable, spcli_args: List = None):
        if not spcli_args:
            spcli_args = []

        results = spcli_attr(*spcli_args)
        parts = results["items"]
        while results["next"]:
            results = self.spapi.next(results)
            parts.extend(results["items"])

        return parts

    def _get_all_songs_pl(self, id_: str) -> List[Song]:
        all_songs = self._get_all(self.spapi.playlist_items, (id_,))

        return list(map(_song_stats, all_songs))

    def _get_all_songs_al(self, id_: str) -> List[Song]:
        all_songs = self._get_all(self.spapi.album_tracks, (id_,))

        return list(map(_song_stats, all_songs))

    def _get_all_playlists(self, executor) -> List[_Songlist]:
        raw_playlists = self._get_all(self.spapi.current_user_playlists)
        results = executor.map(self._plparse, raw_playlists)

        return results

    def _get_all_albums(self, executor) -> List[_Songlist]:
        raw_albums = self._get_all(self.spapi.current_user_saved_albums)
        results = executor.map(self._alparse, raw_albums)

        return results
