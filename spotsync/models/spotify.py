from collections import defaultdict
from typing import Callable, Dict, List, Set

import spotipy
from loguru import logger
from requests.exceptions import ConnectionError, ReadTimeout
from retry import retry
from spotipy.oauth2 import SpotifyOAuth

from ..config import SCOPE, ADList
from .parent import RetryLogger, Song, StateManager, _Songlist


class Spotify(StateManager):
    def __init__(self, config: Dict) -> None:
        super().__init__(config)

        self.spapi = spotipy.Spotify(
            auth_manager=SpotifyOAuth(scope=SCOPE, **self.config["remote"]["auth"])
        )

    def _get_matching_albums(self, adList: ADList) -> Dict[str, Set]:
        logger.debug("Beginning remote album state collection")

        album_name_ids = self._get_all_albums()
        albums = defaultdict(set)

        for a in album_name_ids:
            if adList.allows(a.name) and not adList.denies(a.name):
                # if a.name in deny_list or (
                #     allow_list != set("*") and a.name not in allow_list
                # ):
                #     continue
                albums[a.name].update(self._get_all_songs_al(a.id))

        return dict(albums)

    def _get_matching_playlists(self, adList: ADList) -> Dict[str, Set]:
        logger.debug("Beginning remote playlist state collection")

        playlist_name_ids = self._get_all_playlists()
        playlists = defaultdict(set)

        for p in playlist_name_ids:
            # if p.name in deny_list or (
            #     allow_list != set("*") and p.name not in allow_list
            # ):
            #     continue
            if adList.allows(p.name) and not adList.denies(p.name):
                playlists[p.name].update(self._get_all_songs_pl(p.id))

        return dict(playlists)

    def _get_matching_songs(self, adList: ADList) -> Set:
        logger.debug("Beginning remote song state collection")

        allsongs = self._get_all(self.spapi.current_user_saved_tracks, _song_stats)

        songs = set()
        for song in allsongs:
            # if song.name in deny_list or (
            #     allow_list != set("*") and song.name not in allow_list
            # ):
            #     continue
            if adList.allows(song.name) and not adList.denies(song.name):
                songs.add(song)

        return songs

    @retry(
        (ConnectionError, ReadTimeout),
        tries=4,
        delay=5,
        backoff=2,
        logger=RetryLogger(logger),
    )
    def _get_all(
        self, spcli_attr: Callable, parse_fn: Callable, spcli_args: List = None
    ):
        if not spcli_args:
            spcli_args = []

        results = spcli_attr(*spcli_args)
        parts = results["items"]
        while results["next"]:
            results = self.spapi.next(results)
            parts.extend(results["items"])

        return map(parse_fn, parts)

    def _get_all_songs_pl(self, id_: str) -> List[Song]:
        return self._get_all(self.spapi.playlist_items, _song_stats, (id_,))

    def _get_all_songs_al(self, id_: str) -> List[Song]:
        return self._get_all(self.spapi.album_tracks, _song_stats, (id_,))

    def _get_all_playlists(self) -> List[_Songlist]:
        return self._get_all(
            self.spapi.current_user_playlists, lambda x: _Songlist(x["name"], x["id"])
        )

    def _get_all_albums(self) -> List[_Songlist]:
        return self._get_all(
            self.spapi.current_user_saved_albums,
            lambda x: _Songlist(x["album"]["name"], x["album"]["id"]),
        )


def _song_stats(song_data: Dict) -> Song:
    track = song_data["track"] if "track" in song_data else song_data

    url = track["external_urls"]["spotify"]

    title = track["name"]

    return Song(title, url)
