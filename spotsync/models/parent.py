from abc import ABC, abstractmethod
from collections import namedtuple
from dataclasses import dataclass
from typing import Dict, Set

from ..config import ADList

Song = namedtuple("Song", "name url")
_Songlist = namedtuple("_Songlist", "name id")


@dataclass
class State:
    playlists: Dict[str, Set]
    albums: Dict[str, Set]
    songs: Set[str]
    urlmap: Dict[str, str]

    def __init__(self) -> None:
        ...

    def __repr__(self) -> str:
        urlmap = hasattr(self, "urlmap")
        albumNum = sum(len(self.albums[k]) for k in self.albums)
        playlistNum = sum(len(self.playlists[k]) for k in self.playlists)
        urlMapStr = f", urlmap={len(self.urlmap)}" if urlmap else ""

        return f"State(albums={albumNum}, playlists={playlistNum}, songs={len(self.songs)}{urlMapStr})"


class RetryLogger:
    def __init__(self, logger) -> None:
        self.logger = logger

    def warning(self, warn, *args):
        logstr = warn % args
        self.logger.warning(logstr)


class StateManager(ABC):
    def __init__(self, config: Dict) -> None:
        self.config = config

        self.dispatch = dict(
            playlists=self._get_matching_playlists,
            albums=self._get_matching_albums,
            songs=self._get_matching_songs,
        )

    def state(self) -> State:
        state = State()
        for category, match in self.config["remote"]["to_sync"].items():
            if category not in ["playlists", "albums", "songs"]:
                continue

            results = self.dispatch[category](match)
            setattr(state, category, results)

        if hasattr(self, "urlmap"):
            state.urlmap = self.urlmap

        return state

    @abstractmethod
    def _get_matching_albums(self, adList: ADList) -> Dict[str, Set]:
        ...

    @abstractmethod
    def _get_matching_playlists(self, adList: ADList) -> Dict[str, Set]:
        ...

    @abstractmethod
    def _get_matching_songs(self, adList: ADList) -> Set:
        ...

    def _alparse(self, x):
        return _Songlist(x["album"]["name"], x["album"]["id"])

    def _plparse(self, x):
        return _Songlist(x["name"], x["id"])
