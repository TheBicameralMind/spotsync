from collections import defaultdict
from pathlib import Path
from typing import Dict, Set

import eyed3
from loguru import logger

from ..config import ADList
from .parent import Song, StateManager


# nested_dict = lambda: defaultdict(nested_dict)
def nested_dict():
    return defaultdict(nested_dict)


class Local(StateManager):
    def __init__(self, config: Dict) -> None:
        super().__init__(config)

        self.basedir = Path(self.config["local"]["basedir"])
        self.urlmap = nested_dict()

    def _get_matching_albums(self, adList: ADList) -> Dict[str, Set]:
        logger.debug("Beginning local album state collection")

        base = self.basedir / "albums"

        return self._get_matching_album_playlist(base, adList)

    def _get_matching_playlists(self, adList: ADList) -> Dict[str, Set]:
        logger.debug("Beginning local playlist state collection")

        base = self.basedir / "playlists"

        return self._get_matching_album_playlist(base, adList)

    def _get_matching_songs(self, adList: ADList) -> Set:
        logger.debug("Beginning local song state collection")

        results = set()

        for songname in (self.basedir / "songs").glob("*"):
            # if adList.allows(songname.stem) and not adList.denies(songname.stem):
            # if songname.stem in deny_list or (
            #     allow_list != set("*") and songname.stem not in allow_list
            # ):
            #     continue

            # results.add(songname.stem)
            songdata = eyed3.load(songname)
            title = songdata.tag.title
            url = songdata.tag.audio_source_url.replace("\x00", "")

            results.add(Song(title, url))

            self.urlmap["songs"][url] = songname

        return results

    def _get_matching_album_playlist(
        self, base: Path, adList: ADList
    ) -> Dict[str, Set]:
        results = {}

        for dirname in base.glob("*"):
            # if adList.allows(dirname.stem) and not adList.denies(dirname.stem):
            # if dirname.stem in deny_list or (
            #     allow_list != set("*") and dirname.stem not in allow_list
            # ):
            #     continue

            # results[dirname.stem] = set(map(lambda x: x.stem, dirname.glob("*")))
            results[dirname.stem] = set()
            for song in dirname.glob("*"):
                songdata = eyed3.load(song)
                title = songdata.tag.title
                url = songdata.tag.audio_source_url.replace("\x00", "")

                results[dirname.stem].add(Song(title, url))

                self.urlmap[base.stem][dirname.stem][url] = song

        return results
